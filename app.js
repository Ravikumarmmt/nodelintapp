/* eslint-disable comma-dangle */

/* eslint-disable no-use-before-define */
/* eslint-disable import/no-unresolved */
// npm i express mongoose bcrypt body-parser crypto jsonwebtoken

const express = require('express');

const app = express();
// var bodyParser = require('body-parser');
const mongoose = require('mongoose');

const PORT = 3000;

const db = require('./db');
const UserRoute = require('./routes/user_route');

mongoose.connect(db.DB, { useNewUrlParser: true }).then(
  () => {
    console.log('Database is connected');
  },
  (err) => {
    console.log(`Can not connect to the database${err}`);
  }
);

// const toDataURL = url => fetch(url)
//   .then(response => response.blob())
//   .then(blob => new Promise((resolve, reject) => {
//     const reader = new FileReader()
//     reader.onloadend = () => resolve(reader.result)
//     reader.onerror = reject
//     reader.readAsDataURL(blob)
//   }))

// toDataURL('https://www.satsignal.eu/wxsat/msg-1-fc-40.jpg')
//   .then(dataUrl => {
//     console.log('RESULT:', dataUrl)
//   })

app.use(express.urlencoded({ extended: true }));
// app.use(bodyParser.json());

app.use(express.json({ limit: '10mb', strict: false, verify: rawBodySaver }));

app.use('/user', UserRoute);

app.listen(PORT, () => {
  console.log('Your node js server is running on PORT:', PORT);
});

function rawBodySaver(req, res, buf, encoding) {
  console.log('buf', buf);
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8'); // additional property set on req object
  }

  // console.log("rea", req);
}
