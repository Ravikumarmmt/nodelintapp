/* eslint-disable no-tabs */
const bcrypt = require('bcrypt');
const User = require('../models/user');
// const jwt = require('jsonwebtoken');
const saltRounds = 20;

// const crypto = require('crypto');

exports.add = async (req, res) => {
  try {
    const user = new User(req.body);

    const salt = await bcrypt.genSalt(saltRounds);
    user.password = await bcrypt.hash(user.password, salt);
    const data = await user.save();
    if (data) {
      res.status(200).send(data);
    }
    // user.save()
    //   .then(user => {
    //   res.status(200).json({'user': 'user added successfully'});
    //   })
    //   .catch(err => {
    //   res.status(400).send("unable to save the user into database");
    //   });
  } catch (err) {
    res.status(400).send(err.message);
    // 	for(field in err.errors)
    // 		console.log(err.errors[field].message);
  }
};

// exports.users = (req, res) =>{

// 	user.find(function (err, courses){
// 		// console.log('courses', courses)
// 		// console.log('err', err)
// 		if(err){
// 			res.status(400).send('unable to get data');
// 		}
// 		else {
// 			res.json('courses');
// 		}
// 	});
// };

// exports.user = (req, res) =>{
// 	console.log('getuser');
// 	user.findOne({'email': req.body.email}, function (err, courses){
// 		if(err){
// 			res.status(400).send('unable to get data');
// 		}
// 		else {
// 			res.json(courses);
// 		}
// 	});
// };

// exports.login = async (req, res) =>{
// 	console.log('reqheader', req.header);
// 	console.log('reqBody', req.body);
// 	console.log('rawBody', req.rawBody);
// 	let providedHmac = req.headers['x-shopify-hmac-sha256'];
// 	let shopifyApiKeySecretKey = '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';
// 	console.log('api secret key:', shopifyApiKeySecretKey);
// 	console.log('------ provided hash -----:', providedHmac);
// 	const message = req.rawBody;

// 	const generatedHash = crypto
// 		.createHmac('sha256', shopifyApiKeySecretKey)
// 		.update(message)
// 		.digest('base64');
// 	console.log('------ generatedHash -----:', generatedHash);

// 	res.status(200).send(true);

// 	// let user = await User.findOne({'email': req.body.email});
// 	// if(!user){
// 	//   res.status(401).send("Invalid email and password");
// 	// }
// 	// let validateUser = await bcrypt.compare(req.body.password, user.password);
// 	// // console.log(validateUser);
// 	// if(!validateUser){
// 	//   res.status(401).send("Invalidsd email and password");
// 	// }
// 	// let token = user.generateAuthToken();

// 	// res.status(200).header('x-auth-token', token).send(user);
// 	// User.findOne({'email': req.body.email}, function (err, courses){
// 	//     if(err){
// 	//       res.status(400).send("unable to get data");
// 	//     }
// 	//     else {
// 	//       res.json(courses);
// 	//     }
// 	//   });
// };

// exports.update = (req, res) =>{
// 	//const result = await User.update({_id:id}, {$set:{
// 	//  username:req.body.username,
// 	//    email = req.body.email;
// 	//    age = req.body.age;
// 	// }
// 	// })

// 	//const result = await User.findByIdAndUpdate(id, {$set:{
// 	//  username:req.body.username,
// 	//    email = req.body.email;
// 	//    age = req.body.age;
// 	// }
// 	// })

// 	User.findById(req.params.id, function(err, user) {
// 		if (!user)
// 			return next(new Error('Could not load Document'));
// 		else {
// 			user.username = req.body.username;
// 			user.email = req.body.email;
// 			user.age = req.body.age;

// 			user.save().then(course => {
// 				res.json('Successfully Updated');
// 			})
// 				.catch(err => {
// 					res.status(400).send('unable to update the database');
// 				});
// 		}
// 	});
// };

// exports.delete = (req, res) =>{
// 	User.findByIdAndRemove({_id: req.params.id}, function(err, user){
// 		if(err) res.json(err);
// 		else res.json('Successfully removed');
// 	});
// };
