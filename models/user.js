/* eslint-disable func-names */
const mongoose = require('mongoose');

const { Schema } = mongoose;
const jwt = require('jsonwebtoken');

const UserSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      max: 100,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    test: {
      type: Number,
      required: true,
      validate: {
        validator(v) {
          return v === 1;
        },
        message: 'Value should be one',
      },
    },
    active: {
      type: Number,
      default: 1,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: 'user',
  }
);
UserSchema.methods.generateAuthToken = function () {
  const token = jwt.sign({ id: this.id }, 'jsonwebtokenprivatekey');
  return token;
};
module.exports = mongoose.model('user', UserSchema);
