const express = require('express');

const router = express.Router();

const User = require('../controllers/userController');

router.post('/add', User.add);

router.get('/', User.users);

router.post('/get', User.user);

router.post('/login', User.login);

router.post('/update/:id', User.update);

router.post('/delete/:id', User.delete);

module.exports = router;
